using Honeywell.TestAPI.Core.Contract;
using Honeywell.TestAPI.Core.DAL;
using Honeywell.TestAPI.Core.Model;
using HoneyWell.TestApPI.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;
using Moq;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Honeywell.TestAPI.UnitTests
{
    public class EmployeeTest
    {
        private readonly EmployeeController _controller;
        private readonly Mock<IEmployeeDALManager> mockEmployeeDALManager;
        public EmployeeTest()
        {
            mockEmployeeDALManager = new Mock<IEmployeeDALManager>();
            _controller = new EmployeeController(mockEmployeeDALManager.Object);
        }
        //Insert valid
        [Fact]
        public async Task WhenUserPostValidDataReturnOk()
        {
            mockEmployeeDALManager.Setup(x => x.SaveEmployeeDetails(It.IsAny<Employee>())).Returns(Task.FromResult<bool>(true));
            var result = await _controller.Post(new Employee() {Name="Test", Designation="test", CompanyName="test" }) as ObjectResult;
            Assert.Equal(200, result.StatusCode);
        }
        //Insert Invalid
        [Fact]
        public async Task WhenUserPostInvalidDataReturnBadRequest()
        {
            mockEmployeeDALManager.Setup(x => x.SaveEmployeeDetails(It.IsAny<Employee>())).Returns(Task.FromResult<bool>(false));
            var result = await _controller.Post(new Employee() { Name = "Test", Designation = "test", CompanyName = "test" }) as ObjectResult;
            Assert.Equal(400, result.StatusCode);
        }
        //Get valid
        [Fact]
        public async Task WhenUserRequestforValidDataReturnEmployeedetails()
        {
            mockEmployeeDALManager.Setup(x => x.GetEmployeeDetails(It.IsAny<int>())).Returns(Task.FromResult<Employee>(new Employee() { Id = 1 }));
            var result = await _controller.Get(1) as ObjectResult;
            Assert.Equal(200, result.StatusCode);
        }
        //Get Invalid
        [Fact]
        public async Task WhenUserRequestforInvalidDataReturnNotFound()
        {
            mockEmployeeDALManager.Setup(x => x.GetEmployeeDetails(It.IsAny<int>())).Returns(Task.FromResult<Employee>(null));
            var result = await _controller.Get(1) as ObjectResult;
            Assert.Equal(404, result.StatusCode);
        }
        //Update valid
        [Fact]
        public async Task WhenUserRequestforUpdateValidDataReturnOk()
        {
            mockEmployeeDALManager.Setup(x => x.ModifyEmployeeDetails(It.IsAny<Employee>())).Returns(Task.FromResult<bool>(true));
            var result = await _controller.Put(new Employee() { Name = "Test", Designation = "test", CompanyName = "test" }) as ObjectResult;
            Assert.Equal(200, result.StatusCode);
        }

        //Update valid
        [Fact]
        public async Task WhenUserRequestforUpdateInValidDataReturnNotFound()
        {
            mockEmployeeDALManager.Setup(x => x.ModifyEmployeeDetails(It.IsAny<Employee>())).Returns(Task.FromResult<bool>(false));
            var result = await _controller.Put(new Employee() { Name = "Test", Designation = "test", CompanyName = "test" }) as ObjectResult;
            Assert.Equal(404, result.StatusCode);

        }
    }
}
