﻿using Honeywell.TestAPI.Core.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Honeywell.TestAPI.Core.Contract
{
    public interface IEmployeeDALManager
    {
        public Task<bool> SaveEmployeeDetails(Employee employee);
        public Task<Employee> GetEmployeeDetails(int? id);
        public Task<bool> DeleteEmployeeDetails(int? id);
        public Task<bool> ModifyEmployeeDetails(Employee employee);
    }
}
