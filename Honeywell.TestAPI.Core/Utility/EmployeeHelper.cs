﻿using Honeywell.TestAPI.Core.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Honeywell.TestAPI.Core.Utility
{
    public static class EmployeeHelper
    {
        public static Employee Merge(this Employee employee, Employee employeeTobecopied)
        {
            if (!string.IsNullOrEmpty(employeeTobecopied.Name))
                employee.Name = employeeTobecopied.Name;
            if (!string.IsNullOrEmpty(employeeTobecopied.Address))
                employee.Address = employeeTobecopied.Address;
            if (!string.IsNullOrEmpty(employeeTobecopied.CompanyName))
                employee.CompanyName = employeeTobecopied.CompanyName;
            if (!string.IsNullOrEmpty(employeeTobecopied.Designation))
                employee.Designation = employeeTobecopied.Designation;
            return employee;
        }
    }
}
