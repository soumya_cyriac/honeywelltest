﻿using Honeywell.TestAPI.Core.Contract;
using Honeywell.TestAPI.Core.Model;
using Honeywell.TestAPI.Core.Utility;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Honeywell.TestAPI.Core.DAL
{
    public class EmployeeDALManager: IEmployeeDALManager
    {
        private readonly EmployeeDbContext _context;
        private readonly ILogger<IEmployeeDALManager> _logger;
        public EmployeeDALManager(EmployeeDbContext context,ILogger<IEmployeeDALManager> logger)
        {
            _context = context;
            _logger = logger;
        }
        public async Task<bool> DeleteEmployeeDetails(int?id)
        {
            bool isSuccess = false;
            try
            {
                var employeeDtails = await _context.Employees.FirstOrDefaultAsync(m => m.Id == id);
                if (employeeDtails != null)
                {
                    _context.Employees.Remove(employeeDtails);
                    await _context.SaveChangesAsync();
                    _logger.LogInformation($"Deleted the employee details{id}");
                    isSuccess = true;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Employee details update failed{ex.Message}");
            }
            return isSuccess;
        }

        public async Task<Employee> GetEmployeeDetails(int? id)
        {
            var employeeDtails = await _context.Employees.FirstOrDefaultAsync(m => m.Id == id);
            if (employeeDtails != null)
                return employeeDtails;
            return null;
        }

        public async Task<bool> ModifyEmployeeDetails(Employee employee)
        {
            bool isSuccess = false;
            try
            {
                var employeeDtails = await _context.Employees.FirstOrDefaultAsync(m => m.Id == employee.Id);
                if (employeeDtails != null)
                {
                    employeeDtails = employeeDtails.Merge(employee);
                    _context.Update(employee);
                    await _context.SaveChangesAsync();
                    isSuccess = true;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Employee details update failed{ex.Message}");
            }
            return isSuccess;
        }

        public async Task<bool> SaveEmployeeDetails(Employee employee)
        {
            bool isSuccess = false;
            try
            {
                _context.Add(employee);
                if (await _context.SaveChangesAsync() > 0)
                    isSuccess = true;
                _logger.LogInformation("Employee details added successfully to db");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Employee details adding to db failed{ex.Message}");
            }
            return isSuccess;
        }
    }
}
