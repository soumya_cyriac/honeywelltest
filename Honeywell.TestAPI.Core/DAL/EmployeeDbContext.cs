﻿using Honeywell.TestAPI.Core.Model;
using Microsoft.EntityFrameworkCore;

namespace Honeywell.TestAPI.Core.DAL
{
    public class EmployeeDbContext : DbContext
    {
        public EmployeeDbContext(DbContextOptions options) : base(options)
        {
        }

       public  DbSet<Employee> Employees { get; set; }
    }
}

