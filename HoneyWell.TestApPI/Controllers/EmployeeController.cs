﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Honeywell.TestAPI.Core.Contract;
using Honeywell.TestAPI.Core.DAL;
using Honeywell.TestAPI.Core.Model;
using Honeywell.TestAPI.Core.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace HoneyWell.TestApPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeDALManager _employeeDALManager;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public EmployeeController(IEmployeeDALManager employeeDALManager)
        {
            _employeeDALManager = employeeDALManager;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post(Employee employee)
        {
            if (await _employeeDALManager.SaveEmployeeDetails(employee))
                return Ok("Saved successfully");
            return BadRequest("Failed");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get(int? id)
        {
            var employeeDtails = await _employeeDALManager.GetEmployeeDetails(id);
            if (employeeDtails != null)
                return Ok(employeeDtails);
            return NotFound("No records");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> Delete(int? id)
        {
            if (await _employeeDALManager.DeleteEmployeeDetails(id))
                return Ok("Deleted");
            return NotFound("No records");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="employee"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> Put(Employee employee)
        {
            if (await _employeeDALManager.ModifyEmployeeDetails(employee))
                return Ok("Modified");
            return NotFound("No records");
        }

    }
}
